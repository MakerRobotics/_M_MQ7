/*
 * Library: maker_mq7.h
 *
 * Organization: MakerRobotics
 * Autors: Milan Gigic
 *
 * Date: 24.11.2017.
 * Test: Arduino UNO
 *
 * Note:
 * 
 */


#include "Arduino.h"
#include "maker_mq7.h"
#include "stdint.h"

double R0;

static uint8_t returnSensorValue(uint8_t analogPin)
{
	
	return analogRead(analogPin);
	
}

double maker_returnSensorVolt(uint8_t analogPin)
{
	return (double)returnSensorValue(analogPin)/1024*5.0;
}

double maker_returnRS(uint8_t analogPin)
{
	double SensorVolt=maker_returnSensorVolt(analogPin);
	return (5.0-SensorVolt)/SensorVolt;
}

double maker_returnR0(uint8_t analogPin)
{
	double sensorValue=0;
	for(int x = 0 ; x < 100 ; x++)
    {
        sensorValue = sensorValue + analogRead(analogPin);
    }
    sensorValue = sensorValue/100.0;

    double sensor_volt = sensorValue/1024*5.0;
    double RS = (5.0-sensor_volt)/sensor_volt; 
    R0 = RS/27.0; 
}

double maker_returnPercentage(uint8_t analogPin)
{
	 return maker_returnRS(analogPin)/R0; 
}

