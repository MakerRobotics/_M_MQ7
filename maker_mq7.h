#ifndef _MAKER_MQ7_
#define _MAKER_MQ7_

extern double R0;

double maker_returnSensorVolt(uint8_t analogPin);

double maker_returnRS(uint8_t analogPin);

double maker_returnR0(uint8_t analogPin);

double maker_returnPercentage(uint8_t analogPin);

#endif
